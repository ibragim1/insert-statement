-- Insert the favorite film into the "film" table
INSERT INTO public.film (film_id, title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features, fulltext)
VALUES 
(9999, 'Skyward Bound', 'An epic tale of adventure and discovery in the uncharted skies.', 2023, 1, null, 14, 4.99, 130, 22.99, 'PG', ARRAY['Adventure', 'Drama'], to_tsvector('english', 'Skyward Bound An epic tale of adventure and discovery in the uncharted skies.'))
RETURNING film_id;

-- Insert actors into the "actor" table
INSERT INTO public.actor (first_name, last_name)
VALUES
  ('Alice', 'Windsor'),
  ('Charlie', 'Hawkins'),
  ('Diana', 'Morgan');

-- Get the actor_ids of the newly inserted actors
WITH inserted_actors AS (
   SELECT actor_id, first_name, last_name
   FROM public.actor
   WHERE (first_name, last_name) IN (
     ('Alice', 'Windsor'),
     ('Charlie', 'Hawkins'),
     ('Diana', 'Morgan')
   )
)
-- Associate the new actors with "Skyward Bound" in the "film_actor" table
INSERT INTO public.film_actor (actor_id, film_id)
SELECT actor_id, 9999
FROM inserted_actors;

-- Insert "Skyward Bound" into the inventory of a store
INSERT INTO public.inventory (film_id, store_id, last_update)
VALUES
  (9999, 1, now());
